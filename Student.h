#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
using namespace std;

class Student {
private:
    //strings for names and address
    string name, fullAddress, address, city, state;
    //characters for genders
    char gender, fees;
    //ints for birthdates, addresses, and age
    int month, day, year, zip, age;
    //bools for student fees
    bool feeStatus;
    
public:
    Student();
    ~Student();
    void setInfo();
    void setName();
    void setGender();
    void setAddress();
    void setDate();
    void calculateAge();
    void setPayment();
};


#endif
