#include "Student.h"

Student::Student() {
    string name, fullAddress, address, city, state = " ";
    char gender, fees = ' ';
    int month, day, year, zip, age = 0;
    bool feeStatus = false;
}

Student::~Student(){}

void Student::setInfo() {
    cout << "Enter student data\n\n";
    setName();
    setDate();
    setGender();
    setAddress();
    setPayment();
    
    //record data
    
    fstream record;
    record.open("/Users/Matthew/Desktop/db.txt", fstream::out | fstream::in | fstream::app);
    
    if (record.fail()) {
        cout << "File where data is stored failed to open";
        exit(1);
    }
    
    record << endl;
    record << "Name: " << name << endl;
    record << "Age: " << age << endl;
    
    switch (toupper(gender)) {
        case 'M':
            record << "Gender: Male\n";
            break;
        case 'F':
            record << "Gender: Female\n";
            break;
            
        default:
            break;
    }
    
    record << "Address: " << fullAddress << endl;
    
    record << "Fee Status: ";
    
    if (feeStatus == true) {
        record << "Paid";
    }
    
    else {
        record << "Unpaid";
    }
    
    record << endl << endl << "-------------------------" << endl;
    
    cout << endl << "Data has been added";
    
    record.close();
}

void Student::setName(){
    cout << "What is the student full name: ";
    getline(cin, name);
}

void Student::setGender(){
    cout << "What is the gender of the student: ";
    cin >> gender;
}

void Student::setAddress(){
    cin.ignore();
    cout << "What is the street address: ";
    getline(cin, address);
    cout << "What is the city: ";
    getline(cin, city);
    cout << "What is the state: ";
    getline(cin, state);
    cout << "What is the zip code: ";
    cin >> zip;
    
    fullAddress = address + " " + city + ", " + state + " " + to_string(zip);
}

//setDate is to gather data on the birthday then calculate user age

void Student::setDate() {
    cout << "Enter birth month: ";
    cin >> month;
    
    cout << "Enter birth day: ";
    cin >> day;
    
    cout << "Enter birth year: ";
    cin >> year;
    
    calculateAge();
}

//calculateAge is used to actually do the math on the user age
//-----
//still need to figure out how to make currentDate dynamic

void Student::calculateAge() {
    int month_now = 6;
    int day_now = 1;
    int year_now = 2016;
    
    if (month <= month_now) {
        age = year_now - year;
    }
    
    else if (month > month_now) {
        if (day < day_now) {
            age = year_now - year;
            age--;
        }
        
        else {
            age = year_now - year;
        }
    }
}

//setPayment checks if students have paid there fees via bool statements

void Student::setPayment() {

        cout << "Has the student paid their fees (Y or N): ";
        cin >> fees;
        
        if (toupper(fees) == 'Y') {
            feeStatus = true;
        }
        
        else if (toupper(fees) == 'N') {
            feeStatus = false;
        }
        
        else {
            cout << "\nYou chose an invalid option, try again.\n\n";
            setPayment();
        }
}